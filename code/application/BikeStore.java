// Samin Ahmed 
// 2043024
// Fall 23
package application;
import vehicles.Bicycle; 

public class BikeStore {
    public static void main(String []args){
        Bicycle [] bikes = new Bicycle[4]; 
        bikes[0] = new Bicycle("Alex", 3, 25.42); 
        bikes[1] = new Bicycle("Ben", 4, 10); 
        bikes[2] = new Bicycle("Charlie", 5, 4.42); 
        bikes[3] = new Bicycle("Darwin", 7, 4.32); 


        for (Bicycle bike : bikes){
            System.out.println(bike); 
        }
    }
}
