// Samin Ahmed 
// 2043024
// Fall 23

package vehicles; 
public class Bicycle{

    // Fields 
    private String manufacturer;
    private int numberGears;
    private double maxSpeed; 


    // Constructor 
    public Bicycle(String man, int gears, double max){
        this.manufacturer = man; 
        this.numberGears = gears;
        this.maxSpeed = max; 
    }



    // Getters 
    public String getManufacturer(){
        return this.manufacturer; 
    }

    public int getNumberGears(){
        return this.numberGears; 
    }

    public double getMaxSpeed(){
        return this.maxSpeed; 
    }


// toString
    public String toString(){
        return "Bicycle: " + '\n' +
         "Manufacturer: " + this.manufacturer + '\n' +
         "Number of Gears: " + this.numberGears + '\n' + 
         "Max Speed: " + this.maxSpeed + '\n'; 
    }

}